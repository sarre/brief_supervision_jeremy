#!/bin/bash

echo "
#################################################################################################################################
#                                                                                                                               #
#                                               Infrastructure Load Balancé                                                     #
#                                                                                                                               #
#################################################################################################################################
"

NomDuVnet=vnetbud
IpAddressVnet=192.168.1.0/24
NomDuSubnet=subnetbud
IpAddressSubnet=192.168.1.0/27
NomIpPublic=publicipbud
NomDuLb=lbbud
NomBackendPool=backpoolbud
NomIpBackend=backendipbud
NomHealthProbe=hpbud
NomVm=vmbud
AdminName=bud
AdminPwd=Promo20cloud
PrivateIp=192.168.1.5
FrontendIpName=frontbud
NomHTTPRule=HTTPrulebud
NomNSG=nsgbud
NomNSGRuleHTTP=NSGruleHTTP
NomNic=nicbud
NomNatGateway=NATgate
NomAdminMDB=budbdd
nbzone=3
NomInboundRule1=inboundrulebud1
NomInboundRule2=inboundrulebud2

#################################################################################################################################


Create_groupe(){

echo "Nom du groupe"
read NomDuGroupe

                az group create --location eastus --name $NomDuGroupe

echo -e "${VERT}Groupe de ressource $NomDuGroupe crée !!${NC}"
}

Appel_groupe(){

echo "Nom du groupe ressource"
read NomDuGroupe
}

Create_vnet(){

                az network vnet create -l eastus -g $NomDuGroupe -n $NomDuVnet --address-prefix $IpAddressVnet --subnet-name $NomDuSubnet --subnet-prefix $IpAddressSubnet

echo -e "${VERT}Virtual Network crée !!${NC}"
}

Create_load_balancer(){

        #Ip publique
                az network public-ip create -g $NomDuGroupe --name $NomIpPublic --sku Standard
        #ressource LB
                az network lb create -g $NomDuGroupe --name $NomDuLb --sku Standard --public-ip-address $NomIpPublic --frontend-ip-name $FrontendIpName --backend-pool-name $NomBackendPool
        #sonde integrité
                az network lb probe create -g $NomDuGroupe --lb-name $NomDuLb -n $NomHealthProbe --protocol tcp --port 80
        #règle du LB
                az network lb rule create --resource-group $NomDuGroupe --lb-name $NomDuLb --name $NomHTTPRule --protocol tcp --frontend-port 80 --backend-port 80 --frontend-ip-name $FrontendIpName --backend-pool-name $NomBackendPool --probe-name $NomHealthProbe --disable-outbound-snat true --idle-timeout 15 --enable-tcp-reset true

echo -e "${VERT}LoadBalancer crée  !!${NC}"
}

Create_nsg(){

        #creation groupe
                az network nsg create --resource-group $NomDuGroupe --name $NomNSG
        #regle de securité1
                az network nsg rule create --resource-group $NomDuGroupe --nsg-name $NomNSG --name $NomNSGRuleHTTP --protocol '*' --direction inbound --source-address-prefix '*' --source-port-range '*' --destination-address-prefix '*' --destination-port-range 80 --access allow --priority 200
        #regle de securité2
                az network nsg rule create --resource-group $NomDuGroupe --nsg-name $NomNSG --name NSGRule22 --protocol '*' --direction inbound --source-address-prefix '*' --source-port-range '*' --destination-address-prefix '*' --destination-port-range 22 --access allow --priority 300

echo -e "${VERT}Network Security Groupe crée  !!${NC}"
}

##############################################################################################

Create_nic(){

                az network nic create --resource-group $NomDuGroupe --name $compoNomNic --vnet-name $NomDuVnet --subnet $NomDuSubnet --network-security-group $NomNSG
}

Create_vm(){

                az vm create -g $NomDuGroupe -n $compoNomVm --admin-username $AdminName --admin-password $AdminPwd --image Debian:debian-11:11-gen2:0.20210928.779 --size Standard_D2_v4 --authentication-type password --private-ip-address $PrivateIp --zone $numZone --no-wait --nics $compoNomNic
}

Ajout_BE_Pool(){

                az network nic ip-config address-pool add --address-pool $NomBackendPool --ip-config-name ipconfig1 --nic-name $compoNomNic --resource-group $NomDuGroupe --lb-name $NomDuLb
}

##############################################################################################
##############################################################################################		

CreateVMplus(){

    Create_nsg
    i=1
    while [ $i -le $NombreVm ]
    do
            compoNomVm=$NomVm$i
            AdminName=$AdminName
            AdminPwd=$AdminPwd
            PrivateIp=$PrivateIp$i
            compoNomNic=$NomNic$i
            #distribution des vms par zone
            numZone=$(($i%$nbzone+1))
            echo "Create vm"
            Create_nic
            Create_vm
            Ajout_BE_Pool
            i=$((i+1))
    done

echo -e "${VERT}Les Virtual Machines ont été crées  !!${NC}"
}

##############################################################################################
##############################################################################################

Create_NAT(){

        #IP publique
                az network public-ip create --resource-group $NomDuGroupe --name myNATgatewayIP --sku Standard --zone 1 2 3
        #ressource Passerelle NAT
                az network nat gateway create --resource-group $NomDuGroupe --name $NomNatGateway --public-ip-addresses myNATgatewayIP --idle-timeout 10
        #associer NAT au subnet
                az network vnet subnet update --resource-group $NomDuGroupe --vnet-name $NomDuVnet --name $NomDuSubnet --nat-gateway $NomNatGateway

echo -e "${VERT}NAT gateway crée  !!${NC}"
}

Appel_mariadb(){

echo "Nom du saas Mariadb en minuscule"
read NomMariadb
}

Create_mariadb(){

                az mariadb server create --name $NomMariadb -g $NomDuGroupe --location eastus --admin-user $NomAdminMDB --admin-password Adminpass1 --sku-name GP_Gen5_2 --version 10.3 --ssl-enforcement Disabled
        #ouverture port mariadb saas pour vm NAT
        #recuperation ip NAT et autorisation de l'ip dans la database
IPNATsortant=$(az network public-ip show --resource-group $NomDuGroupe --name myNATgatewayIP | grep -oE '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')
                az mariadb server firewall-rule create --server-name $NomMariadb  --resource-group $NomDuGroupe --name accesDBrule --end-ip-address $IPNATsortant --start-ip-address $IPNATsortant

echo -e "${VERT}Database Azure mariaDB crée  !! ${NC}"

echo "nom hote database azure mariadb "$NomMariadb
echo "nom admin mariadb "$NomAdminMDB
echo "mot de passe admin mariadb Adminpass1"
}

###############################################################################################

Create_inbound_rule(){

	az network lb inbound-nat-rule create -g $NomDuGroupe --lb-name $NomDuLb --backend-pool-name $NomBackendPool --name $NomInboundRule1 --backend-port 22 --protocol Tcp --frontend-ip-name $FrontendIpName --frontend-port-range-start 1100 --frontend-port-range-end 1130
	az network lb inbound-nat-rule create -g $NomDuGroupe --lb-name $NomDuLb --backend-pool-name $NomBackendPool --name $NomInboundRule2 --backend-port 80 --protocol Tcp --frontend-ip-name $FrontendIpName --frontend-port-range-start 1200 --frontend-port-range-end 1230

}

##############################################################################################
##############################################################################################

menu(){

        #regular expression re, ensemble nombre de 0 à 9
re='^[0-9]+$'
        #un peu de couleur bonus
RED='\033[0;31m'
VERT='\033[1;32m'
NC='\033[0m' # No Color
while
        echo "########################################################"
        echo "########  Menu de creation de ressource Azure  #########"
        echo "#  1: Ressource group                                  #"
        echo "#  2: Vnet                                             #"
        echo "#  3: LoadBalancer                                     #"
        echo "#  4: VM avec (NSG, NIC, Ajout au BackendPool du LB)   #"
        echo "#  5: NAT                                              #"
        echo "#  6: Azure Mariadb Database (saas)                    #"
        echo "#  7: Infra entiere (2vms loadbalancées + Mariadb(saas)#"
        echo "#  8: Quitter le menu                                  #"
        echo "########################################################"
	echo "Entrer le numéro associ a votre séléction"
        echo "Votre choix"
read choix

do
    if  [[ $choix =~ $re ]] #check si le choix est un nombre avec le regex $re
        then
            case $choix in
                1) echo "Create ressource group"
                    Create_groupe;;
                2) echo "Create vnet"
                    Appel_groupe
                    Create_vnet;;
                3) echo "Create load balancer"
                    Appel_groupe
                    Create_load_balancer;;
                4) echo "Vm Number"
                    Appel_groupe
                    read NombreVm
		    if [ $NombreVm -le 3 ]
		    then
                        CreateVMplus
                    else
                        echo "Pour les besoins du test veuillez entrer un nombre inferieur ou egal à 3"
                        exit 1
                    fi;;
                5) echo "Creating NAT Gateway"
                        Appel_groupe
                        Create_NAT;;
                6) echo "Creating Azure Mariadb saas"
                        Appel_groupe
                        Appel_mariadb
                        Create_mariadb;;
                7) echo " Creation de l'infrastructure entiere pour 2vms et variables preset"
                        Appel_mariadb
                        Create_groupe
                        NombreVm=2
                        Create_vnet
                        Create_load_balancer
                        CreateVMplus
                        Create_NAT
                        Create_mariadb
			Create_inbound_rule;;
                8) echo "A bientôt"; exit 1;;
                *) echo -e "${RED}Erreur: choix non valide, entrez numéro de choix existant${NC}";;
            esac
        else
        echo -e "${RED}Erreur: choix non valide, veuillez entrer un nombre${NC}" ;
    fi
done
}

menu


